use anyhow::{anyhow, bail};
use base64::{engine::general_purpose::STANDARD as b64, Engine as _};
use eframe::egui;
use egui_data::EguiData;
use egui_data_derive::EguiData;
use flate2::read::GzDecoder;
use flate2::Compression;
use flate2::GzBuilder;
use serde::{Deserialize, Serialize};
use std::io::Read;
use std::io::Write;

const CHARACTER_NAMES: &[&str] = &["Yamato", "Uzume", "Hinome"];
const MAP_NAMES: &[&str] = &[
    "Forest of Awakening",
    "Sunken Relics",
    "Scorching Labyrinth",
    "Ruins of Yamataikoku",
    "Epilogue",
];

/**
 * two versions of top level struct
 * one serializes into the games nested representation
 * the other to a simple json for pretty storage
 */
#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(rename_all = "PascalCase")]
pub struct SaveDataGame {
    #[serde(with = "json_string")]
    main_save_data_json: MainSaveData,
    #[serde(with = "json_string")]
    setting_save_data_json: SettingSaveData,
}

impl SaveDataGame {
    pub fn decode(s: &str) -> Option<Self> {
        let data = b64.decode(s).ok()?;
        let mut gz = GzDecoder::new(&data[4..]);
        let mut s = String::new();
        gz.read_to_string(&mut s).ok()?;
        let data: Self = serde_json::from_str(&s).ok()?;
        Some(data)
    }

    pub fn encode(&self) -> Option<String> {
        let data = serde_json::to_string(self).ok()?;
        let mut gz = GzBuilder::new()
            .operating_system(10)
            .write(Vec::new(), Compression::new(6));
        let bytes = data.as_bytes();
        let len = bytes.len() as u32;
        gz.write_all(bytes).unwrap();
        let mut data = gz.finish().unwrap();
        let len = len.to_le_bytes();
        let mut len = len.to_vec();
        len.append(&mut data);
        Some(b64.encode(len))
    }
}

impl From<SaveDataPretty> for SaveDataGame {
    fn from(data: SaveDataPretty) -> Self {
        SaveDataGame {
            main_save_data_json: data.main_save_data,
            setting_save_data_json: data.setting_save_data,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(rename_all = "PascalCase")]
pub struct SaveDataPretty {
    pub main_save_data: MainSaveData,
    pub setting_save_data: SettingSaveData,
}

impl save_editor::SaveFile for SaveDataPretty {
    fn encode(&self) -> anyhow::Result<Vec<u8>> {
        let data = SaveDataGame::from(self.clone());
        let data = data.encode();
        let data = match data {
            Some(data) => data,
            None => bail!("save conversion failed"),
        };
        Ok(data.into_bytes())
    }

    fn decode(data: &[u8]) -> anyhow::Result<Self> {
        let s = std::str::from_utf8(&data)?;
        let data = SaveDataGame::decode(s)
            .map_or(Err(anyhow!("savefile conversion failed")), |data| Ok(data))?;
        Ok(data.into())
    }
}

impl From<SaveDataGame> for SaveDataPretty {
    fn from(data: SaveDataGame) -> Self {
        SaveDataPretty {
            main_save_data: data.main_save_data_json,
            setting_save_data: data.setting_save_data_json,
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct MainSaveData {
    #[serde(rename = "_saveDataNo")]
    pub save_data_no: i64,
    #[serde(rename = "_saveDataUnitList")]
    pub save_data_unit_list: Vec<SaveDataUnit>,
}

impl EguiData for MainSaveData {
    fn ui(&mut self, ui: &mut egui::Ui, label: &str) {
        ui.collapsing(label, |ui| {
            self.ui_inner(ui, label);
        });
    }

    fn ui_inner(&mut self, ui: &mut egui::Ui, _label: &str) {
        for ele in &mut self.save_data_unit_list {
            radio_collapse(
                ui,
                &mut self.save_data_no,
                ele.data_no,
                &format!("save data {}", ele.data_no),
                |ui| {
                    ele.ui_inner(ui, "");
                },
            );
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
#[serde(rename_all = "PascalCase")]
pub struct SaveDataUnit {
    #[serde(rename = "_dataNo")]
    pub data_no: i64,
    #[serde(rename = "_currentJob")]
    pub current_job: i64,
    #[serde(rename = "_saveDataUnitEachJobList")]
    pub save_data_unit_each_job_list: Vec<SaveDataUnitEachJob>,
    pub is_got_hidden_item_list: Vec<bool>,
    pub is_done_accumulate_attack_action_list: Vec<bool>,
    #[serde(rename = "_beatenEnemyCount")]
    pub beaten_enemy_count: i64,
    #[serde(rename = "_brokenObjectCount")]
    pub broken_object_count: i64,
}

impl EguiData for SaveDataUnit {
    fn ui(&mut self, ui: &mut egui::Ui, label: &str) {
        ui.collapsing(label, |ui| {
            self.ui_inner(ui, label);
        });
    }

    fn ui_inner(&mut self, ui: &mut egui::Ui, _label: &str) {
        for ele in &mut self.save_data_unit_each_job_list {
            radio_collapse(
                ui,
                &mut self.current_job,
                ele.player_job,
                &format!("job{}", ele.player_job),
                |ui| {
                    ele.ui_inner(ui, "");
                },
            );
        }
        self.is_got_hidden_item_list
            .ui_labeled(ui, "hidden items", MAP_NAMES);
        self.is_done_accumulate_attack_action_list.ui_labeled(
            ui,
            "charge attacks",
            CHARACTER_NAMES,
        );
        self.beaten_enemy_count.ui(ui, "enemies killed");
        self.broken_object_count.ui(ui, "objects broken");
    }
}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(deny_unknown_fields)]
#[serde(rename_all = "PascalCase")]
pub struct SaveDataUnitEachJob {
    #[serde(rename = "_playerJob")]
    pub player_job: i64,
    pub player_data: PlayerData,
    pub map_data: MapData,
    #[serde(rename = "_isSawPrologue")]
    pub is_saw_prologue: bool,
    #[serde(rename = "_isCleared")]
    pub is_cleared: bool,
    #[serde(rename = "_highScoreTimeList")]
    pub high_score_time_list: Vec<f64>,
    #[serde(rename = "_clearTimeEachMapList")]
    #[labels(MAP_NAMES)]
    pub clear_time_each_map_list: Vec<i64>,
    #[serde(rename = "_totalPlayTime")]
    pub total_play_time: i64,
    #[serde(rename = "_playTimeForMap")]
    pub play_time_for_map: f64,
    pub is_done_continue: bool,
    pub is_damaged: bool,
}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(deny_unknown_fields)]
pub struct PlayerData {
    #[serde(rename = "_playerStatus")]
    pub player_status: PlayerStatus,
    #[serde(rename = "_currentPlayerJob")]
    pub current_player_job: i64,
    #[serde(rename = "_position")]
    pub position: Position,
    #[serde(rename = "_direction")]
    pub direction: i64,
    #[serde(rename = "_isWithTorii")]
    pub is_with_torii: bool,
    #[serde(rename = "_isBeforeBoss")]
    pub is_before_boss: bool,
}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(deny_unknown_fields)]
pub struct PlayerStatus {
    #[serde(rename = "_key")]
    pub key: String,
    #[serde(rename = "_hp")]
    pub hp: i64,
    #[serde(rename = "_maxHP")]
    pub max_hp: i64,
    #[serde(rename = "_sp")]
    pub sp: i64,
    #[serde(rename = "_maxSP")]
    pub max_sp: i64,
}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(deny_unknown_fields)]
pub struct Position {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(deny_unknown_fields)]
pub struct MapData {
    #[serde(rename = "_currentMapKey")]
    pub current_map_key: String,
    #[serde(rename = "_currentMapSaveDataUnit")]
    pub current_map_save_data_unit: MapSaveDataUnit,
}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(deny_unknown_fields)]
pub struct MapSaveDataUnit {
    #[serde(rename = "_mapKey")]
    pub map_key: String,
    #[serde(rename = "_enemyAreaSaveDatalist")]
    pub enemy_area_save_data_list: Vec<EnemyAreaSaveData>,
    #[serde(rename = "_mapObjectSaveInfo")]
    pub map_object_save_info: Vec<MapObjectSaveInfo>,
    #[serde(rename = "_recoverHPObjectInfoList")]
    pub recover_hp_object_info_list: Vec<RecoverHpObjectInfo>,
    #[serde(rename = "_creatingRecoveryHPObjectNum")]
    pub creating_recovery_hp_object_num: i64,
    #[serde(rename = "_brokenBushNum")]
    pub broken_bush_num: i64,
    #[serde(rename = "_deadPlayerPositionList")]
    pub dead_player_position_list: Vec<Position>,
    #[serde(rename = "_deadPlayerInFinalBossPositionList")]
    pub dead_player_in_final_boss_position_list: Vec<Position>,
}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(deny_unknown_fields)]
pub struct EnemyAreaSaveData {}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(deny_unknown_fields)]
pub struct MapObjectSaveInfo {}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(deny_unknown_fields)]
pub struct RecoverHpObjectInfo {}

#[derive(Serialize, Deserialize, Clone, EguiData)]
#[serde(deny_unknown_fields)]
#[serde(rename_all = "PascalCase")]
pub struct SettingSaveData {
    #[serde(rename = "_isInitialized")]
    pub is_initialized: bool,
    pub should_show_map_time: bool,
    pub should_shake_camera: bool,
    #[serde(rename = "_currentLanguage")]
    pub current_language: i64,
    #[serde(rename = "_isMuteSE")]
    pub is_mute_se: bool,
    #[serde(rename = "_isMuteBGM")]
    pub is_mute_bgm: bool,
    #[serde(rename = "_audioLevel")]
    pub audio_level: i64,
    #[serde(rename = "_dashKeyTypeNo")]
    pub dash_key_type_no: i64,
}

// thanks to https://github.com/serde-rs/serde/issues/994#issuecomment-316895860
mod json_string {
    use serde::de::{self, Deserialize, DeserializeOwned, Deserializer};
    use serde::ser::{self, Serialize, Serializer};
    use serde_json;

    pub fn serialize<T, S>(value: &T, serializer: S) -> Result<S::Ok, S::Error>
    where
        T: Serialize,
        S: Serializer,
    {
        let j = serde_json::to_string(value).map_err(ser::Error::custom)?;
        j.serialize(serializer)
    }

    pub fn deserialize<'de, T, D>(deserializer: D) -> Result<T, D::Error>
    where
        T: DeserializeOwned,
        D: Deserializer<'de>,
    {
        let j = String::deserialize(deserializer)?;
        serde_json::from_str(&j).map_err(de::Error::custom)
    }
}

fn radio_collapse<T: PartialEq, R>(
    ui: &mut egui::Ui,
    selected: &mut T,
    value: T,
    text: &str,
    f: impl FnOnce(&mut egui::Ui) -> R,
) {
    ui.horizontal(|ui| {
        ui.radio_value(selected, value, "");
        ui.collapsing(text, f);
    });
}
