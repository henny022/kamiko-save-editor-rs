use kamiko_save_editor::{SaveDataGame, SaveDataPretty};
use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() <= 3 {
        eprintln!("usage {} <encode|decode> <in file> <out file>", args[0]);
        return;
    }
    let mode = &args[1];
    let in_file = &args[2];
    let out_file = &args[3];
    let data = match fs::read_to_string(in_file) {
        Ok(data) => data,
        Err(e) => {
            eprintln!("error reading file '{}': {}", in_file, e);
            return;
        }
    };
    let result = if mode == "encode" {
        let data: SaveDataPretty = match serde_json::from_str(&data) {
            Ok(data) => data,
            Err(e) => {
                eprintln!("error reading json from file {}: {}", in_file, e);
                return;
            }
        };
        let data = SaveDataGame::from(data);
        data.encode().expect("error while encoding file")
    } else if mode == "decode" {
        let data = match SaveDataGame::decode(&data) {
            Some(data) => data,
            None => {
                eprintln!("bad SaveData file '{}'", in_file);
                return;
            }
        };
        let data = SaveDataPretty::from(data);
        serde_json::to_string_pretty(&data).expect("error while encoding file")
    } else {
        eprintln!("bad mode '{}'", mode);
        return;
    };
    match fs::write(out_file, result) {
        Ok(_) => {}
        Err(e) => {
            eprintln!("error writing file '{}': {}", out_file, e);
            return;
        }
    }
    println!("Done");
}
