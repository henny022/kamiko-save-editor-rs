use kamiko_save_editor::SaveDataPretty;
use save_editor::SaveEditor;

fn main() -> eframe::Result<()> {
    SaveEditor::<SaveDataPretty>::run("Kamiko Save Editor", "SaveData".to_owned())
}
